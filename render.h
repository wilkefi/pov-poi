#ifndef __RENDER_H_
#define __RENDER_H_

#include <mchck.h>

typedef void (render_cb_t)(uint32_t timeElapsed);

void configureRenderTimer(uint32_t ms, render_cb_t *callback);
void resetRenderTimer();
void disableRenderTimer();

void renderTask();

#endif // __RENDER_H_
