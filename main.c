#include <mchck.h>
#include "light_ws2812_cortex.h"
#include "ledstrip.h"
#include "animation.h"
#include "color.h"

int main(void)
{
	timeout_init();
    configureLED();

    animationRainbow();

    while(1) {
        renderTask();
        __asm__("wfi");
    }

	sys_yield_for_frogs();
}
