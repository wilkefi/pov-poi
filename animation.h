#ifndef __ANIMATION_H_
#define __ANIMATION_H_

#include "render.h"
#include "color.h"

void cancelAnimation();
void animationRainbow();
void animationFade(ColorHSB color);

#endif //__ANIMATION_H_
