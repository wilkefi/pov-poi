
#ifndef __LEDSTRIP_H_
#define __LEDSTRIP_H_

#include <mchck.h>

//#define LEDCOUNT 240
#define LEDCOUNT 1
#include "color.h"

void configureLED();
void setLED(ColorDevice color, unsigned int index);
void setLEDRange(ColorDevice color, unsigned int start, unsigned int count);
void setLEDAll(ColorDevice color);

void sendLEDData();

#endif // __LEDSTRIP_H_
