#include "color.h"

#define MIN(x, y) (x < y ? x : y)

ColorDevice fromHSBToDevice(ColorHSB hsbColor)
{
    uint32_t hue = hsbColor.h;
    uint32_t saturation = hsbColor.s;
    uint32_t brightness = hsbColor.b;

    // thrice the number to get the precision we need
    uint32_t deg = 128;
    uint32_t dHue = hue * 3;

    uint32_t hAlt = dHue / deg;
    uint32_t f = (dHue % deg) * 2;
    
    uint8_t p = (brightness * (255 - saturation)) / 255;
    uint8_t q = ((brightness * (255 - ((saturation * f) / 255))) / 255);
    uint8_t t = ((brightness * (255 - (((saturation * (255 - f))) / 255))) / 255);

    uint8_t b = hsbColor.b;
    ColorDevice rgbColor;

    switch(hAlt) {
        case 0:
            rgbColor.r = b;
            rgbColor.g = t;
            rgbColor.b = p;
            break;
        case 1:
            rgbColor.r = q;
            rgbColor.g = b;
            rgbColor.b = p;
            break;
        case 2:
            rgbColor.r = p;
            rgbColor.g = b;
            rgbColor.b = t;
            break;
        case 3:
            rgbColor.r = p;
            rgbColor.g = q;
            rgbColor.b = b;
            break;
        case 4:
            rgbColor.r = t;
            rgbColor.g = p;
            rgbColor.b = b;
            break;
        case 5:
        default:
            rgbColor.r = b;
            rgbColor.g = p;
            rgbColor.b = q;
            break;
    }

    return rgbColor;
}

ColorDevice fromRGBToDevice(ColorRGB rgbColor)
{
    ColorDevice deviceColor;
    deviceColor.r = rgbColor.r;
    deviceColor.g = rgbColor.g;
    deviceColor.b = rgbColor.b;
    return deviceColor;
}
