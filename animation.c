#include "animation.h"
#include "color.h"
#include "ledstrip.h"

static ColorHSB animationHSBColor;

void cancelAnimation()
{
    disableRenderTimer();
}

void doAnimationRainbow(uint32_t ms)
{
    // Do the rainbow in 5 seconds
    // Hue has 256 values so roughly every 20ms
    uint8_t hue = (ms / 20) % 256;
    ColorHSB color;
    color.s = 255;
    color.b = 255;
    color.h = hue;

    ColorDevice deviceColor = fromHSBToDevice(color);
    setLED(deviceColor, 0);
    sendLEDData();
}

void animationRainbow()
{
    configureRenderTimer(20, &doAnimationRainbow);
}

void doAnimationFade(uint32_t ms)
{
    // Do a full fade in roughly 10 seconds
    uint8_t brightness;
    uint32_t step = ms / 20;
    uint32_t brightnessStep = step % 256;
    bool fadeOut = (step % 512) >= 256;

    if (fadeOut) {
        brightness = 255 - brightnessStep;
    } else {
        brightness = brightnessStep;
    }
    ColorHSB color = animationHSBColor;
    color.b = brightness;

    ColorDevice deviceColor = fromHSBToDevice(color);
    setLED(deviceColor, 0);
    sendLEDData();
}

void animationFade(ColorHSB color)
{
    animationHSBColor = color;
    configureRenderTimer(20, &doAnimationFade);
}


