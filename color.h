#ifndef __COLOR_H_
#define __COLOR_H_

#include <mchck.h>

typedef struct __attribute__((__packed__))
{
    uint8_t g;
    uint8_t r;
    uint8_t b;
} ColorDevice;

typedef struct __attribute__((__packed__))
{
    uint8_t r;
    uint8_t g;
    uint8_t b;
} ColorRGB;

// Hue: 0 - 255
// Saturation: 0 - 255
// Brightness: 0 - 255
typedef struct __attribute__((__packed__))
{
    uint8_t h;
    uint8_t s;
    uint8_t b;
} ColorHSB;

ColorDevice fromHSBToDevice(ColorHSB hsbColor);
ColorDevice fromRGBToDevice(ColorRGB rgbColor);

#define ColorDeviceBlack {0, 0, 0}
#define ColorDeviceWhite {255, 255, 255}
#define ColorDeviceRed {0, 255, 0}
#define ColorDeviceGreen {255, 0, 0}
#define ColorDeviceBlue {0, 0, 255}
#define ColorDeviceCyan {255, 0, 255}
#define ColorDeviceYellow {255, 255, 0}
#define ColorDeviceMagenta {0, 255, 255}
#define ColorDeviceOrange {128, 255, 0}
#define ColorDevicePurple {0, 128, 128}
#define ColorDeviceBrown {102, 153, 51};

#define ColorHSBBlack {0, 0, 0}
#define ColorHSBWhite {0, 0, 255}
#define ColorHSBRed {0, 255, 255}
#define ColorHSBGreen {85, 255, 128}
#define ColorHSBBlue {170, 255, 128}
#define ColorHSBCyan {128, 255, 128}
#define ColorHSBYellow {59, 255, 128}
#define ColorHSBMagenta {213, 255, 128}
#define ColorHSBOrange {22, 255, 128}
#define ColorHSBPurple {213, 255, 64}
#define ColorHSBBrown {21, 128, 102}

#endif // __COLOR_H_