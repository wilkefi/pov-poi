#include "ledstrip.h"
#include "light_ws2812_cortex.h"

static ColorDevice ledbuffer[LEDCOUNT];

void configureLED()
{
    gpio_dir(GPIO_PTB0, GPIO_OUTPUT);
}

void setLED(ColorDevice color, unsigned int index)
{
    if (index < LEDCOUNT) {
        ledbuffer[index] = color;
    }
}

void setLEDRange(ColorDevice color, unsigned int start, unsigned int count)
{
    unsigned int index = start;

    while(count > 0 && index < LEDCOUNT) {
        ledbuffer[index] = color;

        count--;
        index++;
    }
}

void setLEDAll(ColorDevice color)
{
    for(unsigned int i = 0; i < LEDCOUNT; i++) {
        ledbuffer[i] = color;
    }
}

void sendLEDData()
{
    ws2812_sendarray((uint8_t *)&ledbuffer, 3 * LEDCOUNT);
}
