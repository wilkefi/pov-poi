#include "render.h"

typedef struct RenderThread RenderThread;

struct RenderThread {
    bool isRendering;
    bool shouldStartRendering;
    bool shouldStop;
    uint32_t timeElapsed;
    uint32_t frameLength;
    render_cb_t *renderCallback;
    struct timeout_ctx timeoutContext;
};

static RenderThread renderThread;

void renderTimerCallback(void *data)
{
    RenderThread *thread = &renderThread;

    if (!thread) {
        return;
    }

    if (thread->isRendering || thread->shouldStartRendering) {
        // skip frame
        thread->timeElapsed += thread->frameLength;
        timeout_add(&(thread->timeoutContext), thread->frameLength, &renderTimerCallback, NULL);
        return;
    }

    if (thread->shouldStop) {
        // clear render thread
        return;
    }

    thread->shouldStartRendering = true;
    timeout_add(&(thread->timeoutContext), thread->frameLength, &renderTimerCallback, NULL);
}

void configureRenderTimer(uint32_t ms, render_cb_t *callback)
{
    RenderThread *thread = &renderThread;

    thread->isRendering = false;
    thread->shouldStop = false;
    thread->timeElapsed = 0;
    thread->frameLength = ms;
    thread->renderCallback = callback;

    renderTimerCallback(NULL);
}

void resetRenderTimer()
{
    RenderThread *thread = &renderThread;
    thread->timeElapsed = 0;
}

void disableRenderTimer()
{
    RenderThread *thread = &renderThread;
    thread->shouldStop = true;
}

void renderTask()
{
    RenderThread *thread = &renderThread;
    if (thread->shouldStartRendering) {
        thread->isRendering = true;
        thread->shouldStartRendering = false;

        thread->renderCallback(thread->timeElapsed);
        thread->timeElapsed += thread->frameLength;

        thread->isRendering = false;
    }
}